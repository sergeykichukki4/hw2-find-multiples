"use strict";

//Mandatory task
let num;
let count = 0;

do {
    num = prompt("Enter integer");
} while (!num || isNaN(num) || !(Number.isInteger(+num)) || +num < 0)

for (let i = 0; i <= num; i++) {
    if (i / 5 >= 1 && i % 5 === 0) {
        console.log(i);
        count++;
    }
}

if (count === 0) {
    console.log(`Sorry, no numbers`);
}

//Extra task
let m;
let n;

do {
    m = prompt("Enter first Integer");
    n = prompt("Enter second Integer");
} while (!m || isNaN(m) || !(Number.isInteger(+m)) || +m < 1 || !n || isNaN(n) || !(Number.isInteger(+n)) || +n < 1 || +m === +n)

if (+m > +n) {
    let swap = +m;
    m = +n;
    n = swap;
}

for (let i = m; i <= n; i++) {
    let flag = 0;

    for (let j = 2; j < i; j++) {
        if (i % j === 0) {
            flag = 1;
            break;
        }
    }

    if (i > 1 && flag === 0) {
        console.log(i);
    }
}






